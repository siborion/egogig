#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

void Dialog::setText(QString val)
{
    ui->label->setText(val);
}

Dialog::~Dialog()
{
    delete ui;
}
