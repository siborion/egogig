#include "balance.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QDebug>

Balance::Balance(QWidget *parent) : QWidget(parent)
{
    QVBoxLayout *vLayout = new QVBoxLayout(this);
    vLayout->setContentsMargins(0, 0, 0, 0);
    QHBoxLayout *hLayout1 = new QHBoxLayout();
    hLayout1->setContentsMargins(0, 0, 0, 0);
    QHBoxLayout *hLayout2 = new QHBoxLayout();
    hLayout2->setContentsMargins(0, 0, 0, 0);
    lLeft =  new QLabel("L");
    lRight = new QLabel("R");
    lLeft->setAlignment(Qt::AlignHCenter);
    left  = new QCheckBox();
    left->setChecked(true);
    left->setMaximumSize(QSize(15, 15));
    right = new QCheckBox();
    right->setChecked(true);
    right->setMaximumSize(QSize(15, 15));
    hLayout1->addWidget(left,  0, Qt::AlignCenter);
    hLayout1->addWidget(right, 0, Qt::AlignCenter);
    hLayout2->addWidget(lLeft,  0, Qt::AlignCenter);
    hLayout2->addWidget(lRight, 0, Qt::AlignCenter);
    vLayout->addLayout(hLayout1);
    vLayout->addLayout(hLayout2);
    connect(left,  SIGNAL(stateChanged(int)), SLOT(slChangeBalance()));
    connect(right, SIGNAL(stateChanged(int)), SLOT(slChangeBalance()));
}

void Balance::slChangeBalance()
{
    BalanceRegim regim=BalanceRegim::soundOff;
    if((left->isChecked()) && (right->isChecked()))
        regim = BalanceRegim::soundBoth;
    if((left->isChecked()) && (!right->isChecked()))
        regim = BalanceRegim::soundLeft;
    if((!left->isChecked()) && (right->isChecked()))
        regim = BalanceRegim::soundRight;
    qDebug()<<left->isChecked()<<right->isChecked()<<"Balance"<<regim;
    emit changeBalance(regim);
}


