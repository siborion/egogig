#include "wavbutton.h"
#include <QHBoxLayout>
#include <QDebug>

WavButton::WavButton(QWidget *parent, QStringList sl) : QWidget(parent)
{
    QHBoxLayout *layout     = new QHBoxLayout(this);
    layout->setContentsMargins(0,0,0,0);
    pbAddFolder.setText(sl.at(0));
    pbAddWav.setText(sl.at(1));
    pbAddMid.setText(sl.at(2));
    pbDelete.setText(sl.at(3));

    layout->addWidget(&pbAddFolder);
    layout->addWidget(&pbAddWav);
    layout->addWidget(&pbAddMid);
    layout->addWidget(&pbDelete);

    connect(&pbAddFolder, SIGNAL(pressed()), SLOT(slPress()));
    connect(&pbAddWav,    SIGNAL(pressed()), SLOT(slPress()));
    connect(&pbAddMid,    SIGNAL(pressed()), SLOT(slPress()));
    connect(&pbDelete,    SIGNAL(pressed()), SLOT(slPress()));

}

void WavButton::slPress()
{
    QObject* obj=QObject::sender();
    if (QPushButton *tb= qobject_cast<QPushButton *>(obj))
    {
        if(tb == (&pbAddFolder))
            sigPress(Action::AddFolder);
        if(tb == (&pbAddWav))
            sigPress(Action::AddFile);
        if(tb == (&pbAddMid))
            sigPress(Action::AddMid);
        if(tb == (&pbDelete))
            sigPress(Action::Delete);
    }
}

void WavButton::setButEnabled(bool en)
{
    pbAddWav.setEnabled(en);
    pbDelete.setEnabled(en);
}



