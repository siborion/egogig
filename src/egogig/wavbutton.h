#ifndef WAVBUTTON_H
#define WAVBUTTON_H

#include <QObject>
#include <QWidget>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>

class WavButton : public QWidget
{
    Q_OBJECT
public:
    explicit WavButton(QWidget *parent = nullptr, QStringList sl= QStringList());
    enum Action {AddFolder, AddFile, AddMid, Delete};
    void setButEnabled(bool en);

private:
    QPushButton pbAddFolder;
    QPushButton pbAddWav;
    QPushButton pbAddMid;
    QPushButton pbDelete;
//    QPushButton pbCheck;

private slots:
//    void slAddFolder();
//    void slAddFile();
//    void slDelete();
    void slPress();

signals:
    void sigAddFolder();
    void sigAddFile();
    void sigDelete();
    void sigPress(WavButton::Action);

public slots:
};

#endif // WAVBUTTON_H
