#ifndef MTREEVIEW_H
#define MTREEVIEW_H

#include <QObject>
#include <QWidget>
#include <QTreeView>
#include <QKeyEvent>
#include <QModelIndex>

class MTreeView : public QTreeView
{

public:
    MTreeView();

private:
    void keyPressEvent(QKeyEvent* event);

signals:


};

#endif // MTREEVIEW_H
