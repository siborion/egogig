#include "mainwindow.h"
#include <QApplication>
#include <QStyleFactory>
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QApplication::setOrganizationName("AMT");
    QApplication::setOrganizationDomain("amtelectronics.com");
    QApplication::setApplicationName("Egogig");

    MainWindow w;
//    a.setStyle(QStyleFactory::create("Fusion"));
    a.setStyle(QStyleFactory::create("windowsvista"));
//    a.setStyle(QStyleFactory::create("Windows"));
    qDebug() << QStyleFactory::keys();
    w.show();
    return a.exec();

}
