#include "myfmod.h"
//#include <QFileInfo>
#include <QFile>
#include <QDebug>

MyFmod::MyFmod(QObject *parent) : QObject(parent)
{
    FMOD_INITFLAGS flags=0;
    FMOD_CREATESOUNDEXINFO exinfo;

    memset(&exinfo, 0, sizeof(FMOD_CREATESOUNDEXINFO));
    exinfo.cbsize = sizeof(FMOD_CREATESOUNDEXINFO);
    curState1 = 0;
    curState2 = 0;

    qDebug()<<FMOD_ErrorString(FMOD_System_Create(&systemFmod));
    qDebug()<<FMOD_ErrorString(FMOD_System_Init(systemFmod, 2, flags, 0));

    timer = new QTimer();
    timer->setInterval(90);
    timer->start();
    connect (timer, SIGNAL(timeout()), SLOT(slTimer()));
}


PlayStatus MyFmod::setFileName(QStringList fileName)
{
    qDebug()<<fileName;

    FMOD_CREATESOUNDEXINFO exinfo1;
    FMOD_CREATESOUNDEXINFO exinfo2;
    FMOD_CHANNELGROUP *group=NULL;
    FMOD_BOOL bPlaying;
    memset(&exinfo1, 0, sizeof(FMOD_CREATESOUNDEXINFO));
    memset(&exinfo2, 0, sizeof(FMOD_CREATESOUNDEXINFO));
    exinfo1.cbsize = sizeof(FMOD_CREATESOUNDEXINFO);
    exinfo2.cbsize = sizeof(FMOD_CREATESOUNDEXINFO);
    bool startPlayBack = false;
    bool startClick = false;


    if(fileName.length()>0)
    {
        if(channelPlayBack)
        {
            FMOD_BOOL bPause;
            qDebug()<<FMOD_ErrorString(FMOD_Channel_IsPlaying(channelPlayBack, &bPlaying));
            if(bPlaying)
            {
                FMOD_Channel_GetPaused(channelPlayBack, &bPause);
                FMOD_Channel_SetPaused(channelPlayBack, !bPause);
                FMOD_Channel_SetPaused(channelClick, !bPause);
                emit changeStatus((!bPause)?eStatusPause:eStatusPlay);
                return (!bPause)?eStatusPause:eStatusPlay;
            }
        }

        QString test = fileName.at(0);
        if(test.right(4)==".wav")
        {
            unsigned int length;
            QFile file1(test);
            file1.open(QIODevice::ReadOnly);
            baBuf1 = file1.readAll();
            file1.close();
            ucBuf1 = baBuf1.constData();
            exinfo1.length = baBuf1.length();
            qDebug()<<"1."<<FMOD_ErrorString(FMOD_System_CreateSound(systemFmod, ucBuf1, FMOD_OPENMEMORY, &exinfo1, &soundPlayBack));
            qDebug()<<test;
            qDebug()<<FMOD_ErrorString(FMOD_System_PlaySound(systemFmod, soundPlayBack, group, true, (&channelPlayBack)));
            qDebug()<<FMOD_ErrorString(FMOD_Channel_SetVolume(channelPlayBack, 1));
            qDebug()<<FMOD_ErrorString(FMOD_Channel_SetPan(channelPlayBack, 0));
//            qDebug()<<FMOD_Channel_SetPaused(channelPlayBack, false);
            startPlayBack = true;
            qDebug()<<FMOD_Sound_GetLength(soundPlayBack, &length, FMOD_TIMEUNIT_MS);
            curState1=1;
            emit changeState1(curState1);
            emit changeDuration1(length);
        }
    }

    if(fileName.length()>1)
    {
        QString test = fileName.at(1);
        if(test.right(4)==".wav")
        {
            QFile file2(test);
            file2.open(QIODevice::ReadOnly);
            baBuf2 = file2.readAll();
            file2.close();
            ucBuf2 = baBuf2.constData();
            exinfo2.length = baBuf2.length();
            qDebug()<<"2."<<FMOD_ErrorString(FMOD_System_CreateStream(systemFmod, ucBuf2, FMOD_OPENMEMORY, &exinfo2, &soundClick));
            qDebug()<<test;
            qDebug()<<FMOD_ErrorString(FMOD_System_PlaySound(systemFmod, soundClick, group, true, &channelClick));
            qDebug()<<FMOD_ErrorString(FMOD_Channel_SetVolume(channelClick, 1));
            qDebug()<<FMOD_ErrorString(FMOD_Channel_SetPan(channelClick, 0));
            startClick = true;
//            qDebug()<<FMOD_ErrorString(FMOD_Channel_SetPaused(channelClick, false));
        }
    }
    if( startPlayBack )
    {
        FMOD_Channel_SetPaused(channelPlayBack, false);
    }
    if( startClick )
    {
        FMOD_Channel_SetPaused(channelClick, false);
    }

    FMOD_Channel_IsPlaying(channelPlayBack, &bPlaying);
    emit changeStatus(bPlaying?eStatusPlay:eStatusStop);
    return bPlaying?eStatusPlay:eStatusStop;
}


void MyFmod::setBalance1(Balance::BalanceRegim regim, quint8 volume)
{
    if(channelPlayBack)
    {
        FMOD_BOOL bPlaying;
        qDebug()<<FMOD_ErrorString(FMOD_Channel_IsPlaying(channelPlayBack, &bPlaying));
        if(bPlaying)
        {
            if(regim==Balance::BalanceRegim::soundOff)
                qDebug()<<FMOD_ErrorString(FMOD_Channel_SetVolume(channelPlayBack, 0));
            else
            {
                float val = volume;
                val /= 100;
                qDebug()<<"realVolume1"<<val;
                qDebug()<<FMOD_ErrorString(FMOD_Channel_SetVolume(channelPlayBack, val));
            }
            if(regim==Balance::BalanceRegim::soundBoth)
                qDebug()<<FMOD_ErrorString(FMOD_Channel_SetPan(channelPlayBack, 0));
            if(regim==Balance::BalanceRegim::soundLeft)
                qDebug()<<FMOD_ErrorString(FMOD_Channel_SetPan(channelPlayBack, -1));
            if(regim==Balance::BalanceRegim::soundRight)
                qDebug()<<FMOD_ErrorString(FMOD_Channel_SetPan(channelPlayBack, 1));
            qDebug()<<"-----------"<<volume;
        }
    }
}

void MyFmod::setBalance2(Balance::BalanceRegim regim, quint8 volume)
{
    if(channelClick)
    {
        FMOD_BOOL bPlaying;
        qDebug()<<FMOD_ErrorString(FMOD_Channel_IsPlaying(channelClick, &bPlaying));
        if(bPlaying)
        {
            if(regim==Balance::BalanceRegim::soundOff)
                qDebug()<<FMOD_ErrorString(FMOD_Channel_SetVolume(channelClick, 0));
            else
            {
                float val = volume;
                val /= 100;
                qDebug()<<"realVolume2"<<val;
                qDebug()<<FMOD_ErrorString(FMOD_Channel_SetVolume(channelClick, val));
            }
            if(regim==Balance::BalanceRegim::soundBoth)
                qDebug()<<FMOD_ErrorString(FMOD_Channel_SetPan(channelClick, 0));
            if(regim==Balance::BalanceRegim::soundLeft)
                qDebug()<<FMOD_ErrorString(FMOD_Channel_SetPan(channelClick, -1));
            if(regim==Balance::BalanceRegim::soundRight)
                qDebug()<<FMOD_ErrorString(FMOD_Channel_SetPan(channelClick, 1));
        }
    }
}

void MyFmod::slTimer()
{
    unsigned int position;
    unsigned int length;
    quint16 curPlayPosition;
    if(channelPlayBack)
    {
        FMOD_Channel_GetPosition(channelPlayBack, &position, FMOD_TIMEUNIT_MS);
        if(curState1 != (position))
        {
            FMOD_Sound_GetLength(soundPlayBack, &length, FMOD_TIMEUNIT_MS);
            curPlayPosition = position*1000/length;
            emit changeState1(position?(curPlayPosition?curPlayPosition:1):0);
            if(position>=length)
            {
                stop1();
                stop2();
            }
        }
        curState1 = (position);
    }

    if(channelClick)
    {
        FMOD_Channel_GetPosition(channelClick, &position, FMOD_TIMEUNIT_MS);
        if(curState2 != (position))
        {
            FMOD_Sound_GetLength(soundClick, &length, FMOD_TIMEUNIT_MS);
            curPlayPosition = position*1000/length;
            emit changeState2(curPlayPosition?curPlayPosition:1);
            if(position>=length)
                stop2();
        }
        curState2 = (position);
    }
}

void MyFmod::stop1()
{
    if(channelPlayBack)
    {
        FMOD_Channel_Stop(channelPlayBack);
        FMOD_Sound_Release(soundPlayBack);
        curState1=0;
        emit changeStatus(eStatusStop);
        emit changeState1(curState1);
    }
}

void MyFmod::stop2()
{
    if(channelClick)
    {
        FMOD_Channel_Stop(channelClick);
        FMOD_Sound_Release(soundClick);
        curState2=0;
    }
}

void MyFmod::setPosition(int posVal)
{
    unsigned int length;
    FMOD_Sound_GetLength(soundPlayBack, &length, FMOD_TIMEUNIT_MS);
    FMOD_Channel_SetPosition(channelPlayBack, posVal*length/1000, FMOD_TIMEUNIT_MS);
    FMOD_Channel_SetPosition(channelClick,    posVal*length/1000, FMOD_TIMEUNIT_MS);
}

