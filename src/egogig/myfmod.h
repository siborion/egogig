#ifndef MYFMOD_H
#define MYFMOD_H

#include <QObject>
#include <QTimer>
#include <fmod.h>
#include <fmod_errors.h>
#include "balance.h"

enum PlayStatus {eStatusStop, eStatusPlay, eStatusPause};

class MyFmod : public QObject
{
    Q_OBJECT
public:
    explicit MyFmod(QObject *parent = nullptr);
    PlayStatus setFileName(QStringList fileName);
    void setBalance1(Balance::BalanceRegim, quint8 volume);
    void setBalance2(Balance::BalanceRegim, quint8 volume);
    void setPosition(int);
//    bool setPause1(bool);
//    bool setPause2(bool);
    void stop1();
    void stop2();


private:
    FMOD_SYSTEM  *systemFmod = NULL;
    FMOD_CHANNEL *channelPlayBack = NULL;
    FMOD_CHANNEL *channelClick = NULL;
    FMOD_SOUND   *soundPlayBack;
    FMOD_SOUND   *soundClick;

    QTimer *timer;
    quint8 curState1;
    quint8 curState2;

    QByteArray baBuf1;
    QByteArray baBuf2;
    const char *ucBuf1;
    const char *ucBuf2;




signals:
    void changeDuration1(unsigned int);
    void changeState1(quint16);
    void changeState2(quint16);
    void changeStatus(PlayStatus);

public slots:

private slots:
    void slTimer();

};

#endif // MYFMOD_H
