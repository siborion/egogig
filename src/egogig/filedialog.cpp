#include "filedialog.h"
#include <QHBoxLayout>

FileDialog::FileDialog()
{
    QBoxLayout* layout = new QHBoxLayout;
    fileEdit = new QLineEdit;
    pbOk     = new QPushButton();
    pbOk->setText("ADD");
    pbCancel = new QPushButton();
    pbCancel->setText("CANCEL");
    layout->addWidget(fileEdit);
    layout->addWidget(pbOk);
    layout->addWidget(pbCancel);
    setLayout(layout);
    connect(pbOk,     SIGNAL(clicked()), this, SLOT(accept()));
    connect(pbCancel, SIGNAL(clicked()), this, SLOT(reject()));
}

void FileDialog::setText(QString txt)
{
    fileEdit->setText(txt);
}

void FileDialog::setMask(QString mask)
{
    fileEdit->setInputMask(mask);
}

QString FileDialog::getText()
{
    return fileEdit->text();
}
