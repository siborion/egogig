#ifndef FILEDIALOG_H
#define FILEDIALOG_H

#include <QObject>
#include <QWidget>
#include <QDialog>
#include <QLineEdit>
#include <QPushButton>

class FileDialog : public QDialog
{
public:
    FileDialog();
    void setText(QString);
    void setMask(QString);
    QString getText();
private:
    QLineEdit* fileEdit;
    QPushButton *pbOk;
    QPushButton *pbCancel;
};

#endif // FILEDIALOG_H
