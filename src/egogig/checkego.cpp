#include <QFile>
#include <QDebug>
#include <QDir>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QHeaderView>
#include "checkego.h"

CheckEgo::CheckEgo(QString inDrive)
{
    QStringList sl;
    QVBoxLayout *layout = new QVBoxLayout;
    setLayout(layout);

    this->setWindowTitle("Error ego");

    tableWidget = new QTableWidget();
    layout->addWidget(tableWidget);

    lb = new QLabel();
    lb->setText("Error count: 0");
    layout->addWidget(lb);

    pbOk = new QPushButton;
    pbOk->setText("OK");
    layout->addWidget(pbOk);

    tableWidget->setColumnCount(1); // Указываем число колонок
    tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    tableWidget->horizontalHeader()->setStretchLastSection(true);

    tableWidget->horizontalHeader()->hide();

    drive = inDrive;

    sl = getDirFiles(drive+"PLAYLIST");
    quint16 i = 0;
    foreach (QString val, sl)
    {
        if(!checkEgo(val))
        {
            tableWidget->insertRow(i);
            tableWidget->setItem(i, 0, new QTableWidgetItem(val));
            i++;
        }
    }
    lb->setText(QString("Error count: %1").arg(i));

    connect(pbOk, SIGNAL(clicked(bool)), SLOT(reject()));


}

QStringList CheckEgo::getDirFiles( const QString& dirName  )
{
    QDir dir( dirName );
//    if ( !dir.exists() ) qFatal( "No such directory : %s", dir.dirName());

    QStringList fileNames;
    QStringList fileList = dir.entryList( QDir::Files );
    for ( QStringList::Iterator fit = fileList.begin(); fit != fileList.end(); ++fit )
        fileNames.append( dir.absolutePath() + "/" + *fit );


    QStringList dirList = dir.entryList( QDir::Dirs );

    dirList.removeAt(dirList.indexOf("."));
    dirList.removeAt(dirList.indexOf(".."));

    for ( QStringList::Iterator dit = dirList.begin(); dit != dirList.end(); ++dit ) {
        QDir curDir = dir;
        curDir.cd( *dit );
        QStringList curList = getDirFiles( curDir.absolutePath() );
        for ( QStringList::Iterator it = curList.begin(); it != curList.end(); ++it ) fileNames.append( QFileInfo(*it).absoluteFilePath() );
    }
    return fileNames;
}

bool CheckEgo::checkEgo(QString fileName)
{
    QFile *file = new QFile();
    QString strFile;

    file->setFileName(fileName);

    if(!fileName.indexOf(".ego"))
        return true;

    if (!file->open(QIODevice::ReadOnly | QIODevice::Text))
        return true;

    while (!file->atEnd())
    {
        strFile = drive+file->readLine();
        strFile.remove(0x0a);
        strFile.remove(0x0d);
        QFileInfo fi;
        fi.setFile(strFile);
        qDebug()<<strFile;
        if(!fi.isFile())
            return false;
    }

    return true;
}

void CheckEgo::slClose()
{
    this->close();
}



