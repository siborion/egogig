#ifndef VOLUME_H
#define VOLUME_H

#include <QObject>
#include <QWidget>
#include <QSlider>
#include <QToolButton>
#include <QLabel>
#include "balance.h"

class Volume : public QWidget
{
    Q_OBJECT
public:
    explicit Volume(QWidget *parent = nullptr);
    void setText(QString);

private:
    QSlider *sliderVolume;
    QToolButton *btSound;
    QLabel *label;

    Balance *balance;

private slots:
    void slChangeBalance(Balance::BalanceRegim);

signals:
    void sigChangeVolume(int);
    void changeBalance(Balance::BalanceRegim, quint8 volume);

public slots:
    void slChangeVolume();
};

#endif // VOLUME_H
