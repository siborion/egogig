#include "playbacktrack.h"
#include <QHBoxLayout>
#include <QDebug>
#include <QDir>


PlayBackTrack::PlayBackTrack(QWidget *parent) : QWidget(parent)
{
    QHBoxLayout *layout     = new QHBoxLayout(this);
    layout->setContentsMargins(0,0,0,0);

    gb = new QGroupBox();
    gb->setTitle("T1 Playback Track");
    QHBoxLayout *layoutGb     = new QHBoxLayout(gb);
    layout->addWidget(gb);

    lineEdit.setReadOnly(true);
    layoutGb->addWidget(&lineEdit);
    checkBox.setText("Play Next");
    layoutGb->addWidget(&checkBox);
    layoutGb->addWidget(&pushButton);
    pushButton.setText("add link");
    pushButton.setEnabled(false);
    this->setEnabled(false);
    connect(&pushButton, SIGNAL(pressed()), SLOT(slKeyPress()));
    connect(&checkBox,   SIGNAL(clicked(bool)), SLOT(slNextPlay()));
}

bool PlayBackTrack::setText(QString text)
{
    lineEdit.setText(text);

    QColor color;
    color = Qt::black;
    QPalette *palette = new QPalette();
    QFileInfo file;
    file.setFile(drive+text.remove("\n"));
    if(!file.isFile())
        color = Qt::red;
    palette->setColor(QPalette::Text, color);
    lineEdit.setPalette(*palette);
    return (color == Qt::black);
}

void PlayBackTrack::setDrive(QString text)
{
    drive = text;
}

void PlayBackTrack::setNextPlay(bool nextPlay)
{
    checkBox.setChecked(nextPlay);
}

void PlayBackTrack::slKeyPress()
{
    emit keyPress();
}

void PlayBackTrack::setAddEnabled(bool addEnabled)
{
    pushButton.setEnabled(addEnabled);
}

QString PlayBackTrack::getText()
{
    return lineEdit.text();
}

bool PlayBackTrack::getNextPlay()
{
    return checkBox.isChecked();
}

void PlayBackTrack::slNextPlay()
{
    emit changeNextPlay();
}

