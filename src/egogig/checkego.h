#ifndef CHECKEGO_H
#define CHECKEGO_H

#include <QObject>
#include <QWidget>
#include <QDialog>
#include <QTableWidget>
#include <QStringList>
#include <QPushButton>
#include <QLabel>

class CheckEgo : public QDialog
{

public:
    CheckEgo(QString drive);

private:
    QTableWidget *tableWidget;
    QStringList getDirFiles( const QString& dirName  ) ;
    bool checkEgo(QString fileName);
    QString drive;
    QPushButton *pbOk;
    QLabel *lb;

private slots:
    void slClose();

};

#endif // CHECKEGO_H
