#include "player.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDebug>
#include <QMessageBox>

Player::Player(QWidget *parent) : QWidget(parent)
{
    QHBoxLayout *layout     = new QHBoxLayout(this);
    layout->setContentsMargins(0,0,0,0);

    sliderManualMove = false;

    curPosition = new QLabel("0.0");
    curPosition->setMinimumWidth(30);
    curPosition->setMaximumWidth(30);

    curSec = new QLabel("sec");
    curSec->setMinimumWidth(20);
    curSec->setMaximumWidth(20);

    gb = new QGroupBox();
    gb->setTitle("T1 Playback Track");
    QVBoxLayout *layoutV     = new QVBoxLayout(gb);

    QHBoxLayout *layoutH      = new QHBoxLayout();
    QHBoxLayout *layoutHCheck = new QHBoxLayout();

    layout->addWidget(gb);

    sliderPosition = new QSlider();
    pbPlay = new QPushButton("Play");
    pbPlay->setEnabled(true);
    pbStop = new QPushButton("Stop");
    sliderPosition->setMinimum(0);
    sliderPosition->setMaximum(1000);
    sliderPosition->setOrientation(Qt::Horizontal);
    sliderPosition->setEnabled(false);

    layoutH->addWidget(pbPlay);
    layoutH->addWidget(pbStop);
    layoutH->addWidget(sliderPosition);
    layoutH->addWidget(curPosition);
    layoutH->addWidget(curSec);

    layoutV->addLayout(layoutH);
    layoutV->addLayout(layoutHCheck);

    mf = new MyFmod();

    connect(sliderPosition,  SIGNAL(sliderMoved(int)),        SLOT(slChangePosition(int)));
    connect(sliderPosition,  SIGNAL(sliderPressed()),         SLOT(slSliderPressed()));
    connect(sliderPosition,  SIGNAL(sliderReleased()),        SLOT(slSliderReleased()));
    connect(pbPlay,          SIGNAL(pressed()),               SLOT(slDoPlay()));
    connect(pbStop,         SIGNAL(released()),              SLOT(slStop()));
    connect(mf,              SIGNAL(changeState1(quint16)),    SLOT(slChangeState(quint16)));
    connect(mf,              SIGNAL(changeDuration1(uint)),   SLOT(slChangeDuration(uint)));
    connect(mf,              SIGNAL(changeStatus(PlayStatus)), SLOT(slChangeStatus(PlayStatus)));
}

void Player::setFileName(QStringList name)
{
    fileNames = name;
}

void Player::slChangePosition(int val)
{
    mf->setPosition(val);
}

void  Player::slChangeDuration(unsigned int val)
{
    duration = val;
    sliderPosition->setEnabled(true);
}

//void  Player::slChangePlayPosition(qint64 val)
//{
//    if(duration!=0)
//    {
//        if(!sliderManualMove)
//            sliderPosition->setValue(val*1000/duration);
//        qDebug()<<val;
//    }
//}

void Player::slDoPlay()
{
    if(playValid)
    {
        PlayStatus status;
        qDebug()<<"slDoPlay1";
        status = mf->setFileName(fileNames);
        qDebug()<<"slDoPlay2";
        setBalance1(balanceRegim1, volume1);
        qDebug()<<"slDoPlay3";
        setBalance2(balanceRegim2, volume2);
        qDebug()<<"slDoPlay()"<<status;
    }
    else
    {
        if(this->objectName() == "ego")
        {
            QMessageBox errMes;
            errMes.setText("Wrong ego-file");
            errMes.exec();
        }
    }

}

void Player::slStop()
{
    sliderPosition->setEnabled(false);
    mf->stop1();
    mf->stop2();
}

void Player::setTitle(QString title)
{
    gb->setTitle(title);
}


void Player::setBalance1(Balance::BalanceRegim regim, quint8 volume)
{
    mf->setBalance1(regim, volume);
    balanceRegim1 = regim;
    volume1 = volume;
    qDebug()<<"volume1"<<volume;
}

void Player::setBalance2(Balance::BalanceRegim regim, quint8 volume)
{
    mf->setBalance2(regim, volume);
    balanceRegim2 = regim;
    volume2 = volume;
    qDebug()<<"volume2"<<volume;
}

void Player::slChangeState(quint16 val)
{
    unsigned int durTmp;
    durTmp = val*duration/100000;
    curPosition->setText(QString::number((float)durTmp/10,'f', 1));

    if(!sliderManualMove)
        sliderPosition->setValue(val);
    sliderPosition->setEnabled(val==0?false:true);
}

void Player::slSliderPressed()
{
    sliderManualMove = true;
}

void Player::slSliderReleased()
{
    sliderManualMove = false;
}

void Player::slChangeStatus(PlayStatus status)
{
    if(status == eStatusPlay)
    {
        pbPlay->setStyleSheet("QPushButton {color: red}");
        pbPlay->setText("Pause");
    }
    if(status == eStatusPause)
    {
        pbPlay->setStyleSheet("QPushButton {color: blue}");
        pbPlay->setText("Play");
    }
    if(status == eStatusStop)
    {
        pbPlay->setStyleSheet("QPushButton {color: black}");
        pbPlay->setText("Play");
    }
}

void Player::setValid(bool valid)
{
    playValid = valid;
}
