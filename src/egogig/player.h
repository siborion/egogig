#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>
#include <QWidget>
#include <QSlider>
#include <QPushButton>
#include <QGroupBox>
#include <QCheckBox>
#include "myfmod.h"
#include "balance.h"

class Player : public QWidget
{
    Q_OBJECT
public:
    explicit Player(QWidget *parent = nullptr);
    void setFileName(QStringList name);
    void setTitle(QString);
    void setValid(bool);

private:
    QLabel *curPosition;
    QLabel *curSec;
    bool sliderManualMove;
    MyFmod *mf;
    QSlider *sliderPosition;
    QPushButton *pbPlay;
    QPushButton *pbStop;
    unsigned int duration;
    QString fileName1;
    QString fileName2;
    QStringList fileNames;
    QGroupBox   *gb;
    QCheckBox  *ch1;
    QCheckBox  *ch2;
    QCheckBox  *ch3;
    QCheckBox  *ch4;
    Balance::BalanceRegim balanceRegim1;
    quint8 volume1;
    Balance::BalanceRegim balanceRegim2;
    quint8 volume2;
    bool   playValid;

signals:

private slots:
    void slChangePosition(int);
    void slChangeDuration(unsigned int);
//    void slChangePlayPosition(qint64);
    void slDoPlay();
    void slStop();
    void setBalance1(Balance::BalanceRegim, quint8 volume);
    void setBalance2(Balance::BalanceRegim, quint8 volume);
    void slChangeState(quint16);
    void slSliderPressed();
    void slSliderReleased();
    void slChangeStatus(PlayStatus);

public slots:
};

#endif // PLAYER_H
