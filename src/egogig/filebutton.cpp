#include "filebutton.h"
#include <QHBoxLayout>
#include <QDebug>

FileButton::FileButton(QWidget *parent, QStringList sl) : QWidget(parent)
{
    QHBoxLayout *layout     = new QHBoxLayout(this);
    layout->setContentsMargins(0,0,0,0);
    pbAddFolder.setText(sl.at(0));
    pbAddFile.setText(sl.at(1));
    pbDelete.setText(sl.at(2));

    layout->addWidget(&pbAddFolder);
    layout->addWidget(&pbAddFile);
    layout->addWidget(&pbDelete);

    connect(&pbAddFolder, SIGNAL(pressed()), SLOT(slPress()));
    connect(&pbAddFile,   SIGNAL(pressed()), SLOT(slPress()));
    connect(&pbDelete,    SIGNAL(pressed()), SLOT(slPress()));

//    if(sl.size()>3)
//    {
        pbCheck.setText(sl.at(3));
        layout->addWidget(&pbCheck);
        connect(&pbCheck, SIGNAL(pressed()), SLOT(slPress()));
//    }
}

void FileButton::slPress()
{
    QObject* obj=QObject::sender();
    if (QPushButton *tb= qobject_cast<QPushButton *>(obj))
    {
        if(tb == (&pbAddFolder))
            sigPress(Action::AddFolder);
        if(tb == (&pbAddFile))
            sigPress(Action::AddFile);
        if(tb == (&pbDelete))
            sigPress(Action::Delete);
        if(tb == (&pbCheck))
            sigPress(Action::Check);
    }
}

void FileButton::setButEnabled(bool en)
{
    pbAddFile.setEnabled(en);
    pbDelete.setEnabled(en);
}



