QT       += core gui widgets

TARGET = egogig

TEMPLATE = app

CONFIG += c11++

RC_FILE     = resources.rc

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    playbacktrack.cpp \
    clicktrack.cpp \
    filebutton.cpp \
    wavbutton.cpp \
    filedialog.cpp \
    player.cpp \
    volume.cpp \
    mtreeview.cpp \
    dialog.cpp \
    myfmod.cpp \
    balance.cpp \
    checkego.cpp

HEADERS += \
        mainwindow.h \
    playbacktrack.h \
    clicktrack.h \
    filebutton.h \
    wavbutton.h \
    filedialog.h \
    player.h \
    volume.h \
    mtreeview.h \
    dialog.h \
    myfmod.h \
    balance.h \
    checkego.h

mac: ICON = ./ico/eg.icns

include( ../../common.pri )
include( ../../app.pri )


win32: LIBS += -lfmod$${LIB_SUFFIX}

linux-g++: LIBS += -lfmod$${LIB_SUFFIX}

mac: LIBS += -lfmod$${LIB_SUFFIX}

RETURN = $$escape_expand(\n\t)

win32 {
DESTDIR_WIN = $${DESTDIR}
DESTDIR_WIN ~= s,/,\\,g
#QMAKE_POST_LINK += dir $$RETURN
QMAKE_POST_LINK += xcopy ..\..\lib.win32\fmod.dll $${DESTDIR_WIN} $$RETURN
QMAKE_POST_LINK += windeployqt.exe  $${DESTDIR}$${TARGET}.exe $$RETURN
}

macx {
QMAKE_POST_LINK += mkdir $${DESTDIR}$${TARGET}.app/Contents/Frameworks $$RETURN
QMAKE_POST_LINK += cp ../../lib.mac/libfmod.dylib $${DESTDIR}$${TARGET}.app/Contents/Frameworks/libfmod.dylib $$RETURN
QMAKE_POST_LINK += cp ../../lib.mac/libfmodL.dylib $${DESTDIR}$${TARGET}.app/Contents/Frameworks/libfmodL.dylib $$RETURN
#QMAKE_POST_LINK += /Users/siborion/Qt/5.12.6/clang_64/bin/macdeployqt $${DESTDIR}$${TARGET}.app -dmg $$RETURN
QMAKE_POST_LINK += macdeployqt $${DESTDIR}$${TARGET}.app -dmg $$RETURN
QMAKE_POST_LINK += mkdir ../../Output $$escape_expand(\\n\\t)
QMAKE_POST_LINK += cp -r $${DESTDIR}egogig.dmg ../../Output/ $$escape_expand(\\n\\t)
}

FORMS += \
    dialog.ui
