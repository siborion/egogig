#include "clicktrack.h"
#include <QHBoxLayout>
#include <QDebug>
#include <QFileInfo>

ClickTrack::ClickTrack(QWidget *parent) : QWidget(parent)
{
    QHBoxLayout *layout     = new QHBoxLayout(this);
    layout->setContentsMargins(0,0,0,0);
    gb = new QGroupBox();
    gb->setTitle("T2 Subtrack (Click)");
    QHBoxLayout *layoutGb     = new QHBoxLayout(gb);
    layout->addWidget(gb);
    lineEdit.setReadOnly(true);
    layoutGb->addWidget(&lineEdit);
    layoutGb->addWidget(&pushButton);
    pushButton.setText("add link");
    pushButton.setEnabled(false);
    this->setEnabled(false);
    connect(&pushButton, SIGNAL(pressed()), SLOT(slKeyPress()));
}

bool ClickTrack::setText(QString text)
{
    lineEdit.setText(text);

    QColor color;
    color = Qt::black;
    QPalette *palette = new QPalette();
    QFileInfo file;
    file.setFile(drive+text.remove("\n"));
    if(!file.isFile() && !text.isEmpty())
        color = Qt::red;
    palette->setColor(QPalette::Text, color);
    lineEdit.setPalette(*palette);
    return (color == Qt::black);
}

void ClickTrack::setDrive(QString text)
{
    drive = text;
}

void ClickTrack::slKeyPress()
{
    emit keyPress();
}

void ClickTrack::setAddEnabled(bool addEnabled)
{
    pushButton.setEnabled(addEnabled);
}

QString ClickTrack::getText()
{
    return lineEdit.text();
}


