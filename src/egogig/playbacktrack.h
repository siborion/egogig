#ifndef PLAYBACKTRACK_H
#define PLAYBACKTRACK_H

#include <QObject>
#include <QWidget>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QGroupBox>

class PlayBackTrack : public QWidget
{
    Q_OBJECT
public:
    explicit PlayBackTrack(QWidget *parent = nullptr);
    bool setText(QString);
    void setDrive(QString);
    void setNextPlay(bool);
    void setAddEnabled(bool);
    QString getText();
    bool getNextPlay();

private:
    QLineEdit   lineEdit;
    QCheckBox   checkBox;
    QPushButton pushButton;
    QGroupBox   *gb;
    QString drive;

private slots:
    void slKeyPress();
    void slNextPlay();

signals:
    void keyPress();
    void changeNextPlay();

public slots:
};

#endif // PLAYBACKTRACK_H
