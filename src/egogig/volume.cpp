#include "volume.h"
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QStyle>
#include <QImage>
#include <QIcon>
#include <QDebug>

Volume::Volume(QWidget *parent) : QWidget(parent)
{
    QVBoxLayout *layout     = new QVBoxLayout(this);

    QHBoxLayout *sliderLayout = new QHBoxLayout();
    sliderVolume = new QSlider();
    sliderVolume->setMinimum(0);
    sliderVolume->setMaximum(100);
    sliderVolume->setValue(10);
    sliderVolume->setOrientation(Qt::Vertical);
    sliderLayout->addWidget(sliderVolume);

    QHBoxLayout *btSoundLayout = new QHBoxLayout();
    btSound = new QToolButton();
    btSound->setIcon(style()->standardIcon(QStyle::SP_MediaVolume));
    btSound->setEnabled(false);
    btSoundLayout->addWidget(btSound);

    label = new QLabel("T");
    label->setAlignment(Qt::AlignHCenter);

    balance = new Balance();

    layout->addLayout(sliderLayout);
    layout->addLayout(btSoundLayout);
    layout->addWidget(label);
    layout->addWidget(balance);
    connect(sliderVolume, SIGNAL(sliderMoved(int)), SLOT(slChangeVolume()));
    connect(balance, SIGNAL(changeBalance(Balance::BalanceRegim)), SLOT(slChangeBalance(Balance::BalanceRegim)));
}

void Volume::slChangeVolume()
{
    balance->slChangeBalance();
}

void Volume::setText(QString val)
{
    label->setText(val);
}

void Volume::slChangeBalance(Balance::BalanceRegim regim)
{
    emit changeBalance(regim, sliderVolume->value());
}

