#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileSystemModel>
#include <QFileIconProvider>
#include "mtreeview.h"
#include <QListWidget>
#include <QComboBox>
#include <QStringListModel>
#include <QLineEdit>
//#include <QRegExp>
#include "playbacktrack.h"
#include "clicktrack.h"
#include "filebutton.h"
#include "wavbutton.h"
#include "filedialog.h"
#include "player.h"
#include "volume.h"
#include "myfmod.h"
#include "checkego.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
//    FMOD_SYSTEM *systemFmod;
    bool bNoRefreshEgo;
    QFileSystemModel *fileModel;
    QFileSystemModel *egoModel;
    MTreeView   *fileTree;
    MTreeView   *egoTree;
    PlayBackTrack *playBackTrack;
    ClickTrack    *clickTrack;
    FileButton    *fileButton;
    WavButton    *fileWavButton;
    CheckEgo      *checkEgo;

    Player        *filePlayer;
    Player        *egoPlayer;
    QStringList getDrivers();
    QStringList getPlayList();
    QComboBox *cbDrive;
    QString getNextFile(QFileInfoList, QString exp, QString start);

    Volume        *fileVolume;
    Volume        *playBackVolume;
    Volume        *clickVolume;

    void setEgoPlayFileName();
    bool isLegalFilePath(QString path);

    bool bChDriveFile, bChDriveEgo;

    QStringList driveReadyFile; //обработанные диски file
    QStringList driveReadyEgo;  //обработанные диски ego

private slots:
    void slFileReload(QString);
    void fileReload(QFileSystemModel *);
    void filePressed(QModelIndex);
//    void fileDoubleClick(QModelIndex);
    void egoClicked(QModelIndex);
    void driveChange(QString);
    void slPlayBackTrackPressed();
    void slClickPressed();
    void save();
    void slFileSystem(FileButton::Action);
    void slFileSystemWav(WavButton::Action);

};

#endif // MAINWINDOW_H
