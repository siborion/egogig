#include "mainwindow.h"
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QDebug>
#include <QHeaderView>
#include <QFileDialog>
#include <dialog.h>
#include <QErrorMessage>
#include <QMessageBox>
#include <QApplication>
#include <QTextCodec>
#include <QSettings>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    this->setWindowTitle("AMT EGOGIG EG-4 v.1.0.10a");
    this->setMinimumWidth(1050);
    this->setMinimumHeight(500);

    cbDrive = new QComboBox();
    cbDrive->addItems(getDrivers());

    QStringList filter;
    fileModel = new QFileSystemModel();
    fileModel->setRootPath("");
    fileModel->iconProvider()->setOptions(QFileIconProvider::DontUseCustomDirectoryIcons);
    filter.append("*.wav");
    filter.append("*.mid");
    fileModel->setNameFilters(filter);
    fileModel->setNameFilterDisables(false);


    egoModel = new QFileSystemModel();
    egoModel->setRootPath("");
    egoModel->iconProvider()->setOptions(QFileIconProvider::DontUseCustomDirectoryIcons);
    filter.append("*.ego");
    egoModel->setNameFilters(filter);
    egoModel->setNameFilterDisables(false);
    egoModel->setReadOnly(false);

    fileTree    = new MTreeView();
    egoTree     = new MTreeView();
    egoTree->setSelectionMode(QTreeView::SingleSelection);

    playBackTrack = new PlayBackTrack();
    clickTrack    = new ClickTrack();
    QStringList sl;
    sl<<"ADD PLAYLIST"<<"ADD SONG"<<"DELETE"<<"CHECK ALL";
    fileButton    = new FileButton(this, sl);
    sl.clear();
    sl<<"ADD FOLDER"<<"ADD WAV"<<"ADD MIDI"<<"DELETE";
    fileWavButton    = new WavButton(this, sl);

    filePlayer    = new Player();
    egoPlayer     = new Player();
    filePlayer->setTitle("WAV Player");
    egoPlayer->setTitle("EGO Player");
    egoPlayer->setObjectName("ego");

    fileVolume    = new Volume();
    playBackVolume= new Volume();
    clickVolume   = new Volume();
    fileVolume->setText("WAV");
    playBackVolume->setText("T1");
    clickVolume->setText("T2");

    driveChange(cbDrive->currentText());

    QHBoxLayout *hLayout     = new QHBoxLayout();
    QVBoxLayout *vLayout     = new QVBoxLayout();
    QVBoxLayout *vLeftLayout = new QVBoxLayout();
    QVBoxLayout *vRightLayout= new QVBoxLayout();
    QWidget *placeholderWidget = new QWidget;
    placeholderWidget->setLayout(vLayout);
    setCentralWidget(placeholderWidget);

    vLeftLayout->addWidget(egoTree);
    vLeftLayout->addWidget(fileButton);
    vLeftLayout->addWidget(playBackTrack);
    vLeftLayout->addWidget(clickTrack);
    vLeftLayout->addWidget(egoPlayer);

    vRightLayout->addWidget(fileTree);
    vRightLayout->addWidget(fileWavButton);
    vRightLayout->addWidget(filePlayer);

    vLayout->addWidget(cbDrive);
    vLayout->addLayout(hLayout);

    hLayout->addWidget(playBackVolume);
    hLayout->addWidget(clickVolume);
    hLayout->addLayout(vLeftLayout);
    hLayout->addLayout(vRightLayout);
    hLayout->addWidget(fileVolume);

    hLayout->setStretch(0, 0);
    hLayout->setStretch(1, 0);

    connect(fileModel,     SIGNAL(directoryLoaded(QString)),    SLOT(slFileReload(QString)));
    connect(egoModel,      SIGNAL(directoryLoaded(QString)),    SLOT(slFileReload(QString)));
    connect(fileTree,      SIGNAL(pressed(QModelIndex)),        SLOT(filePressed(QModelIndex)));
    connect(egoTree,       SIGNAL(pressed(QModelIndex)),        SLOT(egoClicked(QModelIndex)));
    connect(cbDrive,       SIGNAL(currentTextChanged(QString)), SLOT(driveChange(QString)));
    connect(playBackTrack, SIGNAL(keyPress()),                  SLOT(slPlayBackTrackPressed()));
    connect(clickTrack,    SIGNAL(keyPress()),                  SLOT(slClickPressed()));
    connect(playBackTrack, SIGNAL(changeNextPlay()),            SLOT(save()));
    connect(fileButton,    SIGNAL(sigPress(FileButton::Action)),SLOT(slFileSystem(FileButton::Action)));

    connect(fileWavButton, SIGNAL(sigPress(WavButton::Action)),SLOT(slFileSystemWav(WavButton::Action)));

    connect(fileVolume,    SIGNAL(changeBalance(Balance::BalanceRegim, quint8)), filePlayer, SLOT(setBalance1(Balance::BalanceRegim, quint8)));
    connect(playBackVolume,SIGNAL(changeBalance(Balance::BalanceRegim, quint8)), egoPlayer,  SLOT(setBalance1(Balance::BalanceRegim, quint8)));
    connect(clickVolume,   SIGNAL(changeBalance(Balance::BalanceRegim, quint8)), egoPlayer,  SLOT(setBalance2(Balance::BalanceRegim, quint8)));

    egoTree->setColumnWidth(0,250);
    egoTree->setColumnWidth(1,0);
    egoTree->setColumnWidth(2,70);
    egoTree->setColumnWidth(3,70);
    egoTree->setColumnHidden(1, true);

    fileTree->setColumnWidth(0,250);
    fileTree->setColumnWidth(1,0);
    fileTree->setColumnWidth(2,70);
    fileTree->setColumnWidth(3,70);
    fileTree->setColumnHidden(1, true);

    fileVolume->slChangeVolume();
    playBackVolume->slChangeVolume();
    clickVolume->slChangeVolume();

}

void MainWindow::egoClicked(QModelIndex index)
{
    QString sPlayBackLine;
    QString sClickLine;
    bool bNextPlay = false;
    bool bEnable = false;

    fileButton->setButEnabled(!egoModel->filePath(index).right(10).indexOf(":/PLAYLIST")==0);

    if(egoModel->fileInfo(index).isFile())
    {
        QFile file;
        file.setFileName(egoModel->filePath(index));
        if(file.open(QIODevice::ReadOnly))
        {
            QTextCodec *codec = QTextCodec::codecForName("Windows-1251");
            sPlayBackLine = codec->toUnicode(file.readLine());
            sClickLine    = codec->toUnicode(file.readLine());

            bEnable = true;

            if(sPlayBackLine.indexOf(">")==0)
            {
                sPlayBackLine.remove(0,1);
                bNextPlay = true;
            }

            qDebug()<<"fileSize"<<file.size();
            QModelIndex indexTmp;

            indexTmp = egoModel->index(index.row(), 0);
            qDebug()<<indexTmp.isValid();
            qDebug()<<egoModel->setData(indexTmp, "100", Qt::DisplayRole);
            qDebug()<<egoModel->index(index.row(), 1, index.parent()).data(Qt::DisplayRole);
            file.close();
        }
        egoModel->fileInfo(index).refresh();
        qDebug()<<"SIZE"<<egoModel->fileInfo(index).size();
    }

    bool fileValid = true;

    fileValid &= playBackTrack->setText(sPlayBackLine);
    qDebug()<<fileValid;
    playBackTrack->setNextPlay(bNextPlay);
    playBackTrack->setEnabled(bEnable);
    if(playBackTrack->getText().isEmpty())
        clickTrack->setEnabled(false);
    else
        clickTrack->setEnabled(bEnable);
    fileValid &= clickTrack->setText(sClickLine);
    qDebug()<<fileValid;
    egoPlayer->setValid(fileValid);
    setEgoPlayFileName();
}

void MainWindow::filePressed(QModelIndex index)
{
    QStringList sl;
    sl.append(fileModel->fileInfo(index).filePath());
    playBackTrack->setAddEnabled(fileModel->fileInfo(index).isFile() && (fileModel->fileInfo(index).fileName().toLower().indexOf("wav")>0));
    clickTrack->setAddEnabled(fileModel->fileInfo(index).isFile()  && (fileModel->fileInfo(index).fileName().toLower().indexOf("wav")>0));
    filePlayer->setValid(fileModel->fileInfo(index).isFile());
    filePlayer->setFileName(sl);
    QModelIndex tmpIndex;
    tmpIndex = fileModel->index(0,0).child(0,0);
    fileTree->setRowHidden(0, tmpIndex, true);
}

QStringList MainWindow::getDrivers()
{
    QStringList sl;
    QDir val;

#ifdef Q_OS_WIN
    QFileInfoList fil;
    QFileInfo fi;
    fil = QDir::drives();
    foreach (fi, fil)
    {
        sl.append(fi.absolutePath());
    }
#else
    val.cd("/Volumes/");
    QFileInfoList vals = val.entryInfoList(QDir::Dirs);
    qDebug()<<vals;
    foreach(QFileInfo valu, vals)
    {
        QString volumePath;
        volumePath = valu.filePath();
        if( volumePath.indexOf("/.") < 0 ) //only Volumes
        {
            volumePath.append("/");
            sl.append(volumePath);
        }
    }
#endif

#ifdef Q_OS_LINUX
    sl.append(QDir::homePath()+"/");
#endif

    return (sl);
}

QStringList MainWindow::getPlayList()
{
    QStringList sl;
    QDir dir;
    QFileInfo fi;
    dir.setPath(cbDrive->currentText()+"PLAYLIST/");
    dir.setFilter(QDir::Dirs | QDir::Hidden | QDir::NoSymLinks | QDir::NoDotAndDotDot);
    QFileInfoList fil = dir.entryInfoList();
    foreach (fi, fil)
    {
        sl.append(fi.fileName());
    }
    return (sl);
}

void MainWindow::driveChange(QString str)
{
    QModelIndex rootIndex;
    QModelIndex cIndex;

    fileButton->setButEnabled(false);

    clickTrack->setDrive(str);
    playBackTrack->setDrive(str);

    fileTree->setCurrentIndex(cIndex);
    rootIndex = fileModel->index(QDir::cleanPath(str));
    if (rootIndex.isValid())
    {
        fileTree->setModel(fileModel);
        fileTree->setRootIndex(rootIndex);
    }

    egoTree->setCurrentIndex(cIndex);
    rootIndex = egoModel->index(QDir::cleanPath(str));
    if (rootIndex.isValid())
    {
        egoTree->setModel(egoModel);
        egoTree->setRootIndex(rootIndex);
    }
}

void MainWindow::slPlayBackTrackPressed()
{
    QModelIndex index;
    index = fileTree->currentIndex();

    qDebug()<<"fileModel"<<fileModel->fileInfo(index).fileName();

    if(fileModel->fileInfo(index).isFile())
    {
        qDebug()<<"ERROR"<<__FUNCTION__<<__LINE__;

        playBackTrack->setText("/"+fileModel->filePath(index).remove(0, cbDrive->currentText().length())+"\n");
        clickTrack->setEnabled(true);
        playBackTrack->repaint();
        //        egoPlayer->setEnabled(true);
       save();
    }
    else
    {
        qDebug()<<"ERROR"<<__FUNCTION__<<__LINE__;
    }
    qDebug()<<"ERROR"<<__FUNCTION__<<__LINE__;

    setEgoPlayFileName();
    qDebug()<<"slPlayBackTrackPressed()";
//    driveChange(cbDrive->currentText());
}

void MainWindow::slClickPressed()
{
    QModelIndex index;
    index = fileTree->currentIndex();
    if(fileModel->fileInfo(index).isFile())
    {
        clickTrack->setText("/"+fileModel->filePath(index).remove(0, cbDrive->currentText().length())+"\n");
        save();
        clickTrack->repaint();
    }
    setEgoPlayFileName();
    qDebug()<<"slClickPressed()";
}

void MainWindow::save()
{
    QFile file;
    file.setFileName(egoModel->filePath(egoTree->currentIndex()));
    if(file.open(QIODevice::WriteOnly))
    {
        if(playBackTrack->getNextPlay())
            file.write(">");
        QTextCodec *codec = QTextCodec::codecForName("Windows-1251");
        QByteArray ba;
        ba = codec->fromUnicode(playBackTrack->getText());
        file.write(ba);
        ba = codec->fromUnicode(clickTrack->getText());
        file.write(ba);
        file.close();
    }
}

void MainWindow::slFileSystem(FileButton::Action action)
{
    FileDialog dlg;
    QString str, suffix;
    QDir dir;
    QFile file;

    switch(action)
    {
    case FileButton::Action::AddFolder:
        str=cbDrive->currentText()+"PLAYLIST/";
        dir.setPath(str);
        suffix = getNextFile(dir.entryInfoList(), "default(\\d+)", "");
        dlg.setWindowTitle("Add folder");
        dlg.setText(QString("default%1").arg(suffix));
        if(dlg.exec()==QDialog::Accepted)
        {
            if (dir.exists())
                dir.mkdir(dlg.getText());
            else
            {
                dir.mkpath(str+dlg.getText());
                //                driveChange(cbDrive->currentText());
            }
        }
        break;

    case FileButton::Action::AddFile:
        str = (egoModel->filePath(egoTree->currentIndex()));
        if(!str.isEmpty())
        {
            QFileInfo fi;
            fi.setFile(str);
            if(fi.isFile())
                str=fi.path();
            dir.setPath(str);
            suffix = getNextFile(dir.entryInfoList(), "(\\d+).ego", "0");
            dlg.setText(QString("%1.ego").arg(suffix));
            dlg.setMask("00.ego");
            dlg.setWindowTitle("Add file");
            if(dlg.exec()==QDialog::Accepted)
            {
                fi.setFile(str+"/"+dlg.getText());
                if(fi.isFile())
                {
                    QMessageBox errMes;
                    errMes.setText("A song with this name already exists. Action canceled");
                    errMes.exec();
                }
                else
                {
                    file.setFileName(str+"/"+dlg.getText());
                    file.open(QIODevice::WriteOnly);
                    file.close();
                    egoTree->expand(egoTree->currentIndex());
                }
            }
        }
        break;

    case FileButton::Action::Delete:
        str = (egoModel->filePath(egoTree->currentIndex()));
        if(!str.isEmpty())
        {
            dir.setPath(str);
            if (dir.exists())
            {
                Dialog dialog;
                dialog.setWindowTitle("Delete playlist?");
                dialog.setText("Are you sure you want to delete the playlist?");
                if(dialog.exec()==QDialog::Accepted)
                    dir.removeRecursively();
            }
            else
            {
                if(QFileInfo(str).isFile())
                {
                    Dialog dialog;
                    dialog.setWindowTitle("Delete song?");
                    dialog.setText("Are you sure you want to delete the song?");
                    if(dialog.exec()==QDialog::Accepted)
                        dir.remove(str);
                }
            }
            egoClicked(egoTree->currentIndex());
        }
        break;

    case FileButton::Action::Check:
        checkEgo = new CheckEgo(cbDrive->currentText());
        checkEgo->exec();
        break;
    }
}

void MainWindow::slFileSystemWav(WavButton::Action action)
{
    static bool NoMidiMessageAgain = false;

    FileDialog dlg;
    QString str, suffix;
    QDir dir;

    switch(action)
    {
    case WavButton::Action::AddFolder:
        if(!fileTree->currentIndex().isValid())
        {
            dir.setPath(cbDrive->currentText()+"SONGS/");
        }
        else
        {
            dir.setPath(fileModel->filePath(fileTree->currentIndex()));
        }

        suffix = getNextFile(dir.entryInfoList(), "default(\\d+)", "");
        dlg.setWindowTitle("Add folder");
        dlg.setText(QString("default%1").arg(suffix));
        if(dlg.exec()==QDialog::Accepted)
        {
            if (dir.exists())
            {
                dir.mkdir(dlg.getText());
            }
            else
            {
                dir.mkpath(str+dlg.getText());
            }
        }
        break;

    case WavButton::Action::AddFile:
        if(fileTree->currentIndex().isValid())
            str = (fileModel->filePath(fileTree->currentIndex()));
        else
            str = cbDrive->currentText()+"SONGS";

        qDebug()<<"str"<<str;

        if(!str.isEmpty())
        {
            //           QStringList sl;
            QFileInfo fi;
            fi.setFile(str);
            if(fi.isFile())
                str=fi.path();
            dir.setPath(str);

            QStringList filter;
            filter.append("*.wav");
            QFileDialog fDialog;
            fDialog.setNameFilter("*.wav");
            //            sl = fDialog.getOpenFileNames();
            if(fDialog.exec()==QDialog::Accepted)
            {
                QApplication::setOverrideCursor(Qt::WaitCursor);
                foreach (QString fileName, fDialog.selectedFiles())
                {
                    QFileInfo fiTmp;
                    fi.setFile(fileName);
                    str = str+"/"+fi.fileName();
                    fiTmp.setFile(str);

                    if(!isLegalFilePath(fi.fileName()))
                    {
                        QMessageBox errMes;
                        errMes.setText("Wrong name. Only Latin characters, digits and Special Characters (except <>:\"|?*) are allowed. Action canceled");
                        errMes.exec();
                        return;
                    }

                    if(fiTmp.isFile())
                    {
                        Dialog dialog;
                        dialog.setWindowTitle("Replace?");
                        dialog.setText("A file with this name already exists.\r\nDo you want to replace it?");
                        if(dialog.exec()==QDialog::Accepted)
                        {
                            QFile(str).remove();
                            QFile::copy(fileName, str);
                        }
                    }
                    else
                        QFile::copy(fileName, str);
                }
                QApplication::setOverrideCursor(Qt::ArrowCursor);
            }
        }
        break;

    case WavButton::Action::AddMid:
    {
//        QSettings settings;
//        if ( settings.value("NoMidiMessageAgain", false).toBool() == false )
        if( NoMidiMessageAgain == false)
        {
            QMessageBox msgbox;
            QCheckBox *cb = new QCheckBox("Do not show this message again");
            cb->setChecked(false);
            msgbox.setText("To use a midi track just place it in the same folder with your \"Track #1\" WAV file. That midi file must be named as the \"Track #1\" too (for example Song-Track1.wav and Song-Track1.mid). The size of the midi file has to be less than 4 Кb.");
            msgbox.addButton(QMessageBox::Ok);
            msgbox.setCheckBox(cb);
            QObject::connect(cb, &QCheckBox::stateChanged, [](int state){
//                if (static_cast<Qt::CheckState>(state) == Qt::CheckState::Checked) {QSettings settings; settings.setValue("NoMidiMessageAgain", true);} });
            if (static_cast<Qt::CheckState>(state) == Qt::CheckState::Checked) { NoMidiMessageAgain = true;} });
//                NoMidiMessageAgain
            msgbox.exec();
        }

        if(fileTree->currentIndex().isValid())
            str = (fileModel->filePath(fileTree->currentIndex()));
        else
            str = cbDrive->currentText()+"SONGS";

        if(!str.isEmpty())
        {
            QFileInfo fi;
            fi.setFile(str);
            if(fi.isFile())
                str=fi.path();
            dir.setPath(str);

            QStringList filter;
            filter.append("*.mid");
            QFileDialog fDialog;
            fDialog.setNameFilter("*.mid");
            if(fDialog.exec()==QDialog::Accepted)
            {
                QApplication::setOverrideCursor(Qt::WaitCursor);
                foreach (QString fileName, fDialog.selectedFiles())
                {
                    QFileInfo fiTmp;
                    fi.setFile(fileName);
                    str = str+"/"+fi.fileName();
                    fiTmp.setFile(str);

                    if(!isLegalFilePath(fi.fileName()))
                    {
                        QMessageBox errMes;
                        errMes.setText("Wrong name. Only Latin characters, digits and Special Characters (except <>:\"|?*) are allowed. Action canceled");
                        errMes.exec();
                        return;
                    }

                    if(fiTmp.isFile())
                    {
                        Dialog dialog;
                        dialog.setWindowTitle("Replace?");
                        dialog.setText("A file with this name already exists.\r\nDo you want to replace it?");
                        if(dialog.exec()==QDialog::Accepted)
                        {
                            QFile(str).remove();
                            QFile::copy(fileName, str);
                        }
                    }
                    else
                        QFile::copy(fileName, str);
                }
                QApplication::setOverrideCursor(Qt::ArrowCursor);
            }
        }
    }
    break;

    case WavButton::Action::Delete:
        str = (fileModel->filePath(fileTree->currentIndex()));
        if(!str.isEmpty())
        {
            dir.setPath(str);
            if (dir.exists())
            {
                Dialog dialog;
                dialog.setWindowTitle("Delete folder?");
                dialog.setText("Deleting a folder can affect playlists.\r\nAre you sure you want to delete the folder?");
                if(dialog.exec()==QDialog::Accepted)
                    dir.removeRecursively();
            }
            else
            {
                if(QFileInfo(str).isFile())
                {
                    Dialog dialog;
                    dialog.setWindowTitle("Delete file?");
                    dialog.setText("Deleting a file can affect playlists.\r\nAre you sure you want to delete the file?");
                    if(dialog.exec()==QDialog::Accepted)
                        dir.remove(str);
                }
            }
        }
        break;
    }
}


QString MainWindow::getNextFile(QFileInfoList fli, QString exp, QString start)
{
    QString ret;
    QRegExp reg;
    reg.setPattern(exp);
    if(fli.length()<=2)
        ret = start;
    else
    {
        qint16 max=-1;
        foreach (QFileInfo fi, fli)
        {
            fi.fileName();
            if(reg.indexIn(fi.fileName())>=0)
            {
                qint16 tmp;
                tmp = reg.cap(1).toInt();
                if(max<tmp)
                    max=tmp;
            }
        }
        max++;
        ret = QString("%1").arg(max); //, 2, 10, QChar('0'));
    }
    return ret;
}

void MainWindow::setEgoPlayFileName()
{
    QFileInfo fi1, fi2;
    QStringList sl;

    fi1.setFile(cbDrive->currentText()+playBackTrack->getText().remove("\n"));
    fi2.setFile(cbDrive->currentText()+clickTrack->getText().remove("\n"));
    sl.append(fi1.filePath());
    sl.append(fi2.filePath());
    egoPlayer->setFileName(sl);
}

bool MainWindow::isLegalFilePath(QString path)
{
    path = path.toUpper();
    QString illegal="<>:\"|?*";
    foreach (const QChar& c, path)
    {
        if ((c.toLatin1() >= 192) && (c.toLatin1() <= 223))
            return false;
        if (c.toLatin1() < 32)
            return false;
        if (illegal.contains(c))
            return false;
    }
    static QStringList devices;
    if (!devices.count())
        devices << "CON" << "PRN" << "AUX" << "NUL" << "COM0" << "COM1" << "COM2"
                << "COM3" << "COM4" << "COM5" << "COM6" << "COM7" << "COM8" << "COM9" << "LPT0"
                << "LPT1" << "LPT2" << "LPT3" << "LPT4" << "LPT5" << "LPT6" << "LPT7" << "LPT8"
                << "LPT9";

    const QFileInfo fi(path);
    const QString basename = fi.baseName();

    foreach (const QString& s, devices)
        if (basename == s)
            return false;

    return true;
}

void MainWindow::slFileReload(QString val)
{
    if( val.lastIndexOf("/") != (val.length()-1) )
        val.append("/");
    qDebug()<<val<<cbDrive->currentText();
    if(val==cbDrive->currentText())
        fileReload(fileModel);
    if(val==cbDrive->currentText())
        fileReload(egoModel);
}

void MainWindow::fileReload(QFileSystemModel *tb)
{
    QModelIndex rootIndex;
    if (tb)
    {
        rootIndex = tb->index(QDir::cleanPath(cbDrive->currentText()),0);
        if (rootIndex.isValid())
        {
            for(quint8 i=0; i<250; i++)
            {
                if(tb->data(rootIndex.child(i, 0)).isNull())
                {
                    break;
                }
                else
                {
                    if(tb==fileModel)
                    {
                        if(tb->data(rootIndex.child(i, 0)).toString()!="SONGS")
                            fileTree->setRowHidden(i, rootIndex, true);
                        else
                        {
                            fileTree->expand(rootIndex.child(i, 0));
                            fileTree->setCurrentIndex(rootIndex.child(i,0));
                        }
                    }
                    if(tb==egoModel)
                    {
                        if(tb->data(rootIndex.child(i, 0)).toString()!="PLAYLIST")
                            egoTree->setRowHidden(i, rootIndex, true);
                        else
                        {
                            egoTree->expand(rootIndex.child(i, 0));
                            egoTree->setCurrentIndex(rootIndex.child(i,0));
                        }
                    }
                }
            }
        }
    }
}


MainWindow::~MainWindow()
{
}




