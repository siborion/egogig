#ifndef FILEBUTTON_H
#define FILEBUTTON_H

#include <QObject>
#include <QWidget>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>

class FileButton : public QWidget
{
    Q_OBJECT
public:
    explicit FileButton(QWidget *parent = nullptr, QStringList sl= QStringList());
    enum Action {AddFolder, AddFile, Delete, Check};
    void setButEnabled(bool en);

private:
    QPushButton pbAddFolder;
    QPushButton pbAddFile;
    QPushButton pbDelete;
    QPushButton pbCheck;

private slots:
//    void slAddFolder();
//    void slAddFile();
//    void slDelete();
    void slPress();

signals:
    void sigAddFolder();
    void sigAddFile();
    void sigDelete();
    void sigPress(FileButton::Action);

public slots:
};

#endif // FILEBUTTON_H
