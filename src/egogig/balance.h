#ifndef BALANCE_H
#define BALANCE_H

#include <QObject>
#include <QWidget>
#include <QCheckBox>
#include <QLabel>


class Balance : public QWidget
{
    Q_OBJECT
public:
    explicit Balance(QWidget *parent = nullptr);
    enum BalanceRegim
    {
        soundOff, soundLeft, soundRight, soundBoth
    };


private:
    QCheckBox *left;
    QCheckBox *right;

    QLabel *lLeft;
    QLabel *lRight;


signals:
    void changeBalance(Balance::BalanceRegim);

private slots:

public slots:
    void slChangeBalance();
};

#endif // BALANCE_H
