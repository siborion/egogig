#ifndef CLICKTRACK_H
#define CLICKTRACK_H

#include <QObject>
#include <QWidget>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QGroupBox>

class ClickTrack : public QWidget
{
    Q_OBJECT
public:
    explicit ClickTrack(QWidget *parent = nullptr);
    bool setText(QString);
    void setDrive(QString);
    void setAddEnabled(bool);
    QString getText();

private:
    QLineEdit   lineEdit;
    QPushButton pushButton;
    QGroupBox   *gb;
    QString drive;

private slots:
    void slKeyPress();

signals:
    void keyPress();

public slots:
};

#endif // CLICKTRACK_H
